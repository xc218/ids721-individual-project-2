# Use an official Rust image
FROM rust:1.75 as builder

# Create a new empty shell project
RUN USER=root cargo new indiv2
WORKDIR /indiv2

# Copy the manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This is a dummy build to get the dependencies cached
RUN cargo build --release
RUN rm src/*.rs

# Now that the dependencies are built, copy your source code
COPY ./src ./src

# Build for release
RUN rm ./target/release/deps/indiv2*
RUN cargo build --release

# test
RUN cargo test -v

# Final stage
FROM debian:bookworm-slim
COPY --from=builder /indiv2/target/release/indiv2 .
ENV ROCKET_ADDRESS=0.0.0.0
CMD ["./indiv2"]
