# Ids721 Individual Project 2

### Requirement:
1. Simple REST API/web service in Rust
2. Dockerfile to containerize service
3. CI/CD pipeline files

### Steps:
#### Setting Docker:
1. Download Docker Desktop for Mac
2. Install Docker Desktop
3. Run Docker Desktop
4. Verify Installation. Run `docker --version` to check that Docker has been installed successfully.
5. Run `docker run hello-world` to download a test image and run it in a container, which verifies that Docker is working correctly on your system.

#### Creating Simple Rust Actix Web App
1. `cargo new indiv2`
2. `cd indiv2`
3. Add `actix-web` to your `Cargo.toml` under `[dependencies]`
4. Replace the content of `src/main.rs` with a simple Actix web server
 ```
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
async fn greet() -> impl Responder {
    HttpResponse::Ok().body("Hello, Actix!")
}
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
 ```
5. Create a Dockerfile
 ```
# Use an official Rust image
FROM rust:1.75 as builder

# Create a new empty shell project
RUN USER=root cargo new indiv2
WORKDIR /indiv2

# Copy the manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This is a dummy build to get the dependencies cached
RUN cargo build --release
RUN rm src/*.rs

# Now that the dependencies are built, copy your source code
COPY ./src ./src

# Build for release
RUN rm ./target/release/deps/indiv2*
RUN cargo build --release

# test
RUN cargo test -v

# Final stage
FROM debian:bookworm-slim
COPY --from=builder /indiv2/target/release/indiv2 .
ENV ROCKET_ADDRESS=0.0.0.0
CMD ["./indiv2"]

 ```
 6. `cargo build`
 7. Build the Docker Image
 `docker build -t indiv2 .
`
8. Run the Container Locally
`docker run -p 8080:8080 indiv2`

9. Open Localhost:8080
![screenshot](image/screen.png)

 #### CI/CD
 Create the .gitlab-ci.yml file. The Docker in Docker (docker:dind) service is used here to allow another Docker daemon to be started inside the GitLab Runner's Docker container. By encapsulating all the build and test tasks within the Dockerfile, the CI/CD pipeline only needs to build and run the Docker container. If any test cases failed, the pipeline will provide the error message.

 ```
 image: docker:25.0.3

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:dind

stages:
  - test

test:
  stage: test
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t indiv2 .
    - docker run -d -p 8080:8080 indiv2
    - docker ps -a

 ```

 ### Demo Video
 The demo video is in the root directory

